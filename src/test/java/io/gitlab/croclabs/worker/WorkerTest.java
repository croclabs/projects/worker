package io.gitlab.croclabs.worker;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class WorkerTest {
	private final Task<String> t1 = new Task<>() {
		@Override
		public String call() throws Exception {
			System.out.println("1");
			Thread.sleep(5000);
			return "Done";
		}
	};

	private final Task<String> t2 = new Task<>() {
		@Override
		public String call() throws Exception {
			System.out.println("2");
			Thread.sleep(5000);
			return "Done";
		}
	};

	private final Task<String> t3 = new Task<>() {
		@Override
		public String call() throws Exception {
			System.out.println("3");
			Thread.sleep(5000);
			return "Done";
		}
	};

	private final Task<String> t4 = new Task<>() {
		@Override
		public String call() throws Exception {
			System.out.println("4");
			Thread.sleep(5000);
			return "Done";
		}
	};

	private final Task<String> t5 = new Task<>() {
		@Override
		public String call() throws Exception {
			System.out.println("5");
			Thread.sleep(5000);
			return "Done";
		}
	};

	private final Task<String> te = new Task<>() {
		@Override
		public String call() throws Exception {
			System.out.println("Exception");
			Thread.sleep(5000);
			throw new WorkerException("Exception");
		}
	};

	@Test
	void testMaxConcurrent() throws Exception {
		Worker worker = Worker
				.multiThread()
				.noDuplicates()
				.maxConcurrentTasks(2);

		worker.add(t1);
		worker.add(t1).exceptionally(throwable -> {
			assertNotNull(throwable);
			return "Exception";
		});
		Thread.sleep(50);
		worker.add(t2);
		Thread.sleep(50);
		worker.add(t3);
		Thread.sleep(50);
		worker.add(t4);
		Thread.sleep(50);
		worker.add(t5);
		Thread.sleep(50);

		Thread.sleep(1000);

		assertEquals(2, worker.getRunningAmount());
		assertEquals(3, worker.getQueuedAmount());
		assertEquals(1, worker.getRejectedAmount());
		assertEquals(1, worker.getFailedAmount());

		Thread.sleep(5000);

		assertEquals(2, worker.getRunningAmount());
		assertEquals(1, worker.getQueuedAmount());
		assertEquals(1, worker.getRejectedAmount());
		assertEquals(1, worker.getFailedAmount());

		Thread.sleep(5000);

		assertEquals(1, worker.getRunningAmount());
		assertEquals(0, worker.getQueuedAmount());
		assertEquals(1, worker.getRejectedAmount());
		assertEquals(1, worker.getFailedAmount());

		Thread.sleep(5000);

		assertEquals(0, worker.getRunningAmount());
		assertEquals(0, worker.getQueuedAmount());
		assertEquals(1, worker.getRejectedAmount());
		assertEquals(1, worker.getFailedAmount());
	}

	@Test
	void testSynchronized() throws Exception {
		Map<Task<?>, String> keys = new ConcurrentHashMap<>();

		Worker worker = Worker
				.multiThread()
				.noDuplicates()
				.synchronize(keys::get);

		keys.put(t1, "1");
		keys.put(t2, "1");
		keys.put(t3, "2");
		keys.put(te, "2");
		keys.put(t5, "2");

		worker.add(t1);
		Thread.sleep(50);
		worker.add(t2);
		Thread.sleep(50);
		worker.add(t3);
		Thread.sleep(50);
		worker.add(te).exceptionally(throwable -> {
			assertNotNull(throwable);
			return "Exception";
		});
		Thread.sleep(50);
		worker.add(t5);

		Thread.sleep(1000);

		assertEquals(2, worker.getRunningAmount());
		assertEquals(3, worker.getQueuedAmount());
		assertEquals(0, worker.getRejectedAmount());
		assertEquals(0, worker.getFailedAmount());

		Thread.sleep(20000);

		assertEquals(0, worker.getRunningAmount());
		assertEquals(0, worker.getQueuedAmount());
		assertEquals(0, worker.getRejectedAmount());
		assertEquals(1, worker.getFailedAmount());
	}
}
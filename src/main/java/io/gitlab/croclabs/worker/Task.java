package io.gitlab.croclabs.worker;

import java.util.concurrent.Callable;

/**
 * Abstract task implementation of a {@link Callable}.
 * A task instance should only be added to ONE worker
 * at a time. Doing otherwise can lead to unexpected
 * behavior because of house holding and synchronization
 * logic inside the task and the worker.
 * <br><br>
 * All fields in the abstract class are transient, so
 * they get reset if you decide to serialize instances
 * of this class.
 *
 * @param <T> The return type of the task
 */
public abstract class Task<T> implements Callable<T> {
	transient boolean acquired; // NOSONAR
	transient boolean locked; // NOSONAR
	transient boolean queued; // NOSONAR
	transient boolean running; // NOSONAR
	transient boolean rejected; // NOSONAR
	transient boolean failed; // NOSONAR
	transient Exception exception; // NOSONAR

	public boolean isAcquired() {
		return acquired;
	}

	public boolean isLocked() {
		return locked;
	}

	public boolean isQueued() {
		return queued;
	}

	public boolean isRunning() {
		return running;
	}

	public boolean isRejected() {
		return rejected;
	}

	public boolean isFailed() {
		return failed;
	}

	public Exception getException() {
		return exception;
	}
}

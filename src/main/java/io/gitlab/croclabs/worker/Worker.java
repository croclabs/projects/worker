package io.gitlab.croclabs.worker;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Logger;

/**
 * A worker implementation that is backed by {@link ThreadPoolExecutor}. The
 * following features are supported:
 *
 * <ul>
 *     <li>single thread</li>
 *     <li>multiple threads</li>
 *     <li>task synchronization</li>
 *     <li>concurrent task execution limitation</li>
 * </ul>
 *
 * A worker will ALWAYS shut down completely via a shutdown hook, letting all supplied tasks finish
 * before shutting down.
 * <br><br>
 * You also get the possibility to serialize the queued tasks to re-add them after restart. Make sure
 * that your supplied {@link Task Tasks} are {@link java.io.Serializable Serializable}. This
 * only makes sense for singlethreaded workers, since multithreaded pools do not have a queue (tho the
 * worker holds a reference of the queued and running tasks).
 */
public final class Worker {
	@Nonnull
	private Collection<Task<?>> queued;
	@Nonnull
	private Collection<Task<?>> running;
	@Nullable
	private Function<Task<?>, Object> synchronization = null;
	@Nullable
	private Semaphore semaphore;
	@Nonnull
	private final ThreadPoolExecutor pool;
	@Nonnull
	private final List<Consumer<Worker>> beforeShutdownConsumers = new ArrayList<>();
	@Nonnull
	private final List<Consumer<Worker>> afterShutdownConsumers = new ArrayList<>();
	private boolean multiThreaded;
	private boolean noDuplicates;
	private long queuedAmount;
	private long runningAmount;
	private long failedAmount;
	private long rejectedAmount;
	private final LoadingCache<Object, ReentrantLock> locks = CacheBuilder
			.newBuilder()
			.expireAfterAccess(5, TimeUnit.MINUTES)
			.build(new CacheLoader<>() {
				@Nonnull
				@Override
				public ReentrantLock load(@Nonnull Object key) {
					return new ReentrantLock(true);
				}
			});

	private Worker(@Nonnull Collection<Task<?>> queued, @Nonnull Collection<Task<?>> running) {
		this(queued, running, new ThreadPoolExecutor(0, 1, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>()));
	}

	private Worker(@Nonnull Collection<Task<?>> queued, @Nonnull Collection<Task<?>> running, @Nonnull ThreadPoolExecutor pool) {
		super();
		this.pool = pool;
		this.queued = Collections.synchronizedCollection(queued);
		this.running = Collections.synchronizedCollection(running);
		shutdownHook();
	}

	/**
	 * Adds a task to this worker. If a {@link Set} is chosen for
	 * the queued tasks collection a failed {@link CompletableFuture}
	 * is returned containing a {@link WorkerException}.
	 *
	 * @param task the task to be added
	 * @return CompletableFuture containing the result of the task for
	 * further execution
	 * @param <T> the type of the task's return value
	 */
	@Nonnull
	public <T> CompletableFuture<T> add(@Nullable Task<T> task) {
		if (task == null) {
			return CompletableFuture.failedFuture(new RejectedExecutionException("Could not set task to queued. It was null."));
		}

		if (!queue(task, true)) {
			RejectedExecutionException ree = new RejectedExecutionException("Could not set task to queued. It might already be in the queue while no duplicates are allowed.");
			reject(task, ree);
			return CompletableFuture.failedFuture(ree);
		}

		return CompletableFuture.supplyAsync(doTask(task), pool);
	}

	/**
	 * Creates the supplier for the {@link CompletableFuture}.
	 *
	 * @param task the current task
	 * @return a supplier to be used to call the task
	 * @param <T> the type of the task's return value
	 */
	@Nonnull
	private <T> Supplier<T> doTask(@Nonnull Task<T> task) {
		return () -> {
			try {
				return call(task);
			} catch (InterruptedException e) {
				cleanOnException(task, e);
				Thread.currentThread().interrupt();
				throw new WorkerException(e.getClass().getName() + ": " + e.getMessage(), e);
			} catch (Exception e) {
				cleanOnException(task, e);
				throw new WorkerException(e.getClass().getName() + ": " + e.getMessage(), e);
			}
		};
	}

	/**
	 * Cleans collections and locks on exception.
	 * If task could be removed from running collection,
	 * the semaphore and the lock are released/unlocked (if necessary).
	 * If task could not be removed from running collection it
	 * has to be in the queued collection (bc it never started)
	 * and is removed from the queued collection instead.
	 *
	 * @param task the task that threw an exception
	 */
	private void cleanOnException(@Nonnull Task<?> task, @Nonnull Exception e) {
		fail(task, e);

		if (task.running) {
			run(task, false);
		}

		if (task.queued) {
			queue(task, false);
		}

		if (task.locked) {
			unlock(task);
		}

		if (task.acquired) {
			release(task);
		}
	}

	private void reject(@Nonnull Task<?> task) {
		task.rejected = true;
		adjustRejectedAmount(1);
	}

	private void reject(@Nonnull Task<?> task, @Nonnull Exception e) {
		task.rejected = true;
		adjustRejectedAmount(1);
		fail(task, e);
	}

	private void fail(@Nonnull Task<?> task, Exception e) {
		task.failed = true;
		task.exception = e;
		adjustFailedAmount(1);
	}

	private boolean queue(@Nonnull Task<?> task, boolean add) {
		if (add) {
			if (this.queued.add(task)) {
				task.queued = true;
				adjustQueuedAmount(1);
				return true;
			} else {
				return false;
			}
		} else {
			if (this.queued.remove(task)) {
				task.queued = false;
				adjustQueuedAmount(-1);
				return true;
			} else {
				return false;
			}
		}
	}

	private boolean run(@Nonnull Task<?> task, boolean add) {
		if (add) {
			if (this.running.add(task)) {
				task.running = true;
				adjustRunningAmount(1);
				return true;
			} else {
				return false;
			}
		} else {
			if (this.running.remove(task)) {
				task.running = false;
				adjustRunningAmount(-1);
				return true;
			} else {
				return false;
			}
		}
	}

	/**
	 * Calls the current task, locks or/and acquires locks/permits
	 * and performs actions on the collections
	 *
	 * @param task the current task
	 * @return the result of the task
	 * @param <T> the type of the return value of the task
	 * @throws Exception might be thrown by the task
	 */
	@Nullable
	private <T> T call(@Nonnull Task<T> task) throws Exception {
		lock(task);
		acquire(task);

		if (!run(task, true)) {
			queue(task, false);
			reject(task);
			throw new RejectedExecutionException("Could not set task to running. It might already be running while no duplicates are allowed.");
		}

		queue(task, false);
		T value = task.call();
		run(task, false);

		release(task);
		unlock(task);

		return value;
	}

	/**
	 * Tries to lock the {@link ReentrantLock} for the given task
	 *
	 * @param task the current task
	 */
	private void lock(@Nonnull Task<?> task) {
		if (multiThreaded && synchronization != null) {
			try {
				locks.get(synchronization.apply(task)).lock();
			} catch (ExecutionException e) {
				throw new WorkerException("Exception locking lock", e);
			}
			task.locked = true;
		}
	}

	/**
	 * Unlocks the {@link ReentrantLock} for the given task
	 *
	 * @param task the current task
	 */
	private void unlock(@Nonnull Task<?> task) {
		if (multiThreaded && synchronization != null) {
			try {
				locks.get(synchronization.apply(task)).unlock();
			} catch (ExecutionException e) {
				throw new WorkerException("Exception unlocking lock", e);
			}
			task.locked = false;
		}
	}

	/**
	 * Acquires a permit from the semaphore, if present and
	 * multithreaded
	 */
	private void acquire(@Nonnull Task<?> task) throws InterruptedException {
		if (multiThreaded && semaphore != null) {
			semaphore.acquire();
			task.acquired = true;
		}
	}

	/**
	 * Releases a permit from the semaphore, if present and
	 * multithreaded
	 */
	private void release(@Nonnull Task<?> task) {
		if (multiThreaded && semaphore != null) {
			semaphore.release();
			task.acquired = false;
		}
	}

	/**
	 * Creates a new singlethreaded worker, backed by {@link ArrayList ArrayLists}
	 * for queued and running tasks.
	 *
	 * @return this worker
	 */
	@Nonnull
	public static Worker singleThread() {
		return singleThread(new ArrayList<>(), new ArrayList<>());
	}

	/**
	 * Creates a new singlethreaded worker with the specified
	 * collections as backing collections for running and
	 * queued tasks. Those collections are made synchronized.
	 *
	 * @param queued The collection used to hold queued tasks
	 * @param running The collection used to hold running tasks
	 * @return this worker
	 */
	@Nonnull
	private static Worker singleThread(@Nonnull Collection<Task<?>> queued, @Nonnull Collection<Task<?>> running) {
		return new Worker(queued, running);
	}

	/**
	 * Creates a new multithreaded worker, backed by {@link ArrayList ArrayLists}
	 * for queued and running tasks.
	 *
	 * @return this worker
	 */
	@Nonnull
	public static Worker multiThread() {
		return multiThread(new ArrayList<>(), new ArrayList<>());
	}

	/**
	 * Creates a new multithreaded worker with the specified
	 * collections as backing collections for running and
	 * queued tasks. Those collections are made synchronized.
	 *
	 * @param queued The collection used to hold queued tasks
	 * @param running The collection used to hold running tasks
	 * @return this worker
	 */
	@Nonnull
	private static Worker multiThread(@Nonnull Collection<Task<?>> queued, @Nonnull Collection<Task<?>> running) {
		Worker worker = new Worker(
				queued,
				running,
				new ThreadPoolExecutor(0, Integer.MAX_VALUE, 1, TimeUnit.MINUTES, new SynchronousQueue<>())
		);
		worker.multiThreaded = true;
		return worker;
	}

	/**
	 * Replaces the queued and running collections with synchronized
	 * {@link LinkedHashSet LinkedHashSets}. Only use this when creating
	 * the worker. Doing differently might result in task loss.
	 *
	 * @return this worker
	 */
	@Nonnull
	public Worker noDuplicates() {
		this.queued = Collections.synchronizedCollection(new LinkedHashSet<>());
		this.running = Collections.synchronizedCollection(new LinkedHashSet<>());
		this.noDuplicates = true;
		return this;
	}

	/**
	 * Adds synchronization mechanics for this worker. The synchronization object
	 * needs to be supplied by the calling code. The execution order for waiting
	 * tasks is not fair.
	 *
	 * @param synchronization Function to obtain a synchronization key for the given task
	 * @return this worker
	 */
	@Nonnull
	public Worker synchronize(@Nonnull Function<Task<?>, Object> synchronization) {
		this.synchronization = synchronization;
		return this;
	}

	/**
	 * Sets the max concurrent threads of this worker. Only
	 * used if multithreaded.
	 *
	 * @param maxTasks The maximum tasks that can be running at a time
	 * @return this worker
	 */
	@Nonnull
	public Worker maxConcurrentTasks(int maxTasks) {
		this.semaphore = new Semaphore(maxTasks, true);
		return this;
	}

	/**
	 * Performed on shutdown
	 */
	private void shutdownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			beforeShutdownConsumers.forEach(c -> c.accept(this));
			this.pool.shutdown();

			try {
				if (!this.pool.awaitTermination(1, TimeUnit.HOURS)) {
					Logger.getLogger(this.getClass().getName()).info("Shutdown timeout exceeded");
				}
			} catch (InterruptedException e) {
				Logger.getLogger(this.getClass().getName()).severe("Shutdown interrupted");
				Thread.currentThread().interrupt();
			}

			afterShutdownConsumers.forEach(c -> c.accept(this));
		}));
	}

	/**
	 * Code executed before the worker shuts down in the shutdown
	 * hook. Use this to e.g. get the queued tasks to serialize them later and to
	 * drain the tasks queued in the pool. Multiple actions can be supplied
	 *
	 * @param beforeShutdown Action to be performed before shutdown
	 * @return this worker
	 */
	@Nonnull
	public Worker beforeShutdown(@Nonnull Consumer<Worker> beforeShutdown) {
		this.beforeShutdownConsumers.add(beforeShutdown);
		return this;
	}

	/**
	 * Code executed after the worker shuts down in the shutdown
	 * hook. Use this to e.g. serialize the tasks gotten by the before
	 * action. Multiple actions can be supplied
	 *
	 * @param afterShutdown Action to be performed after shutdown
	 * @return this worker
	 */
	@Nonnull
	public Worker afterShutdown(@Nonnull Consumer<Worker> afterShutdown) {
		this.afterShutdownConsumers.add(afterShutdown);
		return this;
	}

	/**
	 * Drains all tasks queued in the pool. This does NOT change
	 * the queued or running collections.
	 *
	 * @return collection of drained tasks
	 */
	public Collection<Runnable> drain() {
		List<Runnable> tasks = new ArrayList<>();
		this.pool.getQueue().drainTo(tasks);
		return tasks;
	}

	/**
	 * Drains all tasks queued in the pool. This does NOT change
	 * the queued or running collections.
	 *
	 * @param collection collection to drain tasks to
	 */
	public void drain(@Nonnull Collection<Runnable> collection) {
		this.pool.getQueue().drainTo(collection);
	}

	/**
	 * Drains all tasks queued in the worker. This does ALSO drain
	 * the queued tasks of the pool!
	 *
	 * @return collection of drained tasks
	 */
	public Collection<Task<?>> drainQueued() {
		drain();
		List<Task<?>> tasks = new ArrayList<>(queued);
		queued.clear();
		return tasks;
	}

	/**
	 * Drains all tasks queued in the worker. This does ALSO drain
	 * the queued tasks of the pool!
	 *
	 * @param collection collection to add all queued tasks to
	 */
	public void drainQueued(@Nonnull Collection<Task<?>> collection) {
		drain();
		collection.addAll(queued);
		queued.clear();
	}

	/**
	 *
	 * @return An unmodifiable version of the synchronized queued tasks collection
	 */
	@Nonnull
	public Collection<Task<?>> getQueued() {
		return Collections.unmodifiableCollection(queued);
	}

	/**
	 *
	 * @return An unmodifiable version of the synchronized running tasks collection
	 */
	@Nonnull
	public Collection<Task<?>> getRunning() {
		return Collections.unmodifiableCollection(running);
	}

	@Nonnull
	public ThreadPoolExecutor getPool() {
		return this.pool;
	}

	@Nullable
	public Semaphore getSemaphore() {
		return semaphore;
	}

	public boolean isMultiThreaded() {
		return multiThreaded;
	}

	public boolean isNoDuplicates() {
		return noDuplicates;
	}

	public synchronized long getQueuedAmount() {
		return queuedAmount;
	}

	public synchronized void adjustQueuedAmount(long amount) {
		this.queuedAmount += amount;
	}

	public synchronized long getRunningAmount() {
		return runningAmount;
	}

	public synchronized void adjustRunningAmount(long amount) {
		this.runningAmount += amount;
	}

	public synchronized long getFailedAmount() {
		return failedAmount;
	}

	public synchronized void adjustFailedAmount(long amount) {
		this.failedAmount += amount;
	}

	public synchronized long getRejectedAmount() {
		return rejectedAmount;
	}

	public synchronized void adjustRejectedAmount(long amount) {
		this.rejectedAmount += amount;
	}
}
